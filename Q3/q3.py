def a():
    a=[]
    with open("scores.txt") as file:
        for line in file : 
            details=line.replace("\n","").split(" ")
            total= int(details[2])+int(details[3])+int(details[4]);
            percentage=total/3;
            if(percentage > 90):
                grade="A"
            elif(percentage > 80):
                grade="B"
            elif(percentage > 60):
                grade="C"
            elif(percentage > 50):
                grade="D"
            else : 
                grade="F"
            a.append([details[0],details[1],total,percentage,grade])
    return a;

def b():
    passed=0
    ansa=a()
    for i in ansa:
        if(i[3] > 50):
            passed+=1
    return passed/len(ansa)

def c():
    ansa= a()
    lowest,highest= 301,-301
    for i in ansa: 
        if(lowest > i[2]):
            lowest= i[2]
        if(highest < i[2]):
            highest = i[2]
    return lowest,highest

def d():
    ansa = a()
    dictionary={
        "A":0,
        "B":0,
        "C":0,
        "D":0,
        "F":0
    }
    for i in ansa:
        if(i[4]=="A"):
            dictionary["A"]+=1
        elif(i[4]=="B"):
            dictionary["B"]+=1
        elif(i[4]=="C"):
            dictionary["C"]+=1
        elif(i[4]=="D"):
            dictionary["D"]+=1
        elif(i[4]=="F"):
            dictionary["F"]+=1
    return dictionary


if __name__ == "__main__":               # __name__ returns the name of module. This is the statement from which this program will run
     print(a()) 
     print(b())
     print(c())
     print(d())
        
